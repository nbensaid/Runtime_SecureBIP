#!/bin/bash
# 
# Licensed to Institut National de Recherche en Informatique et Automatique
# - INRIA - one one or more license agreement.
# 
# All right reserved.
# 
# @copyright 2017,2018 Inria
# 


SECBIP_DIR=$(dirname $(realpath $0))/./SecureBIP/
GEN_DIR=./generated/

TIMEOUT=15
SNAPSHOT=5

BIPC=${SECBIP_DIR}/bip-full/
SECBIP_JAR=SecureBIPSynthesis.jar

error() {
    echo 'Error...' "$*"
    exit 1
}

loadEnv() {
    if test x"${BIP2_ENGINE_LIB_DIR}" == x""; then
        test -d ${BIPC} -a -e ${BIPC}/setup.sh  || error "BIP not found"
        if pushd ${BIPC} > /dev/null; then
            source ./setup.sh
            popd > /dev/null
        else
            error "Could not load BIP environment"
        fi
    fi

    test -d ${SECBIP_DIR}  || error "Application directory not found"
    test -e ${SECBIP_DIR}/${SECBIP_JAR}  || error "Application not found"

    if ! test -e ${SECBIP_DIR}/instrumentation/dlmCompare.hpp ; then
        pushd ${SECBIP_DIR} > /dev/null
        jar -xvf ${SECBIP_JAR} instrumentation/{bip.stg,dlmCompare.hpp,dlmCompare.cpp} > /dev/null \
            || error "Could not install application"
        popd > /dev/null
    fi

    echo "Secure BIP environment loaded"
}

runSecureBip() {
    local BIP_DIR=$1
    local BIPP=$2
    local BIPD=$3
    local OUTPUT=$4

    test x"${BIP2_ENGINE_LIB_DIR}" != x"" || error "BIP environment not loaded"
    test -e "${BIP_DIR}/${BIPP}.bip" || error "BIP file or directory not found"
    test -d "${OUTPUT}" || mkdir -p "${OUTPUT}" || error "Could not generate output directory"

    export SECBIP_DIR
    if test "${BIP_DIR}/${BIPP}.bip" -nt ${OUTPUT}/secureBip.bip; then
        java -jar ${SECBIP_DIR}/${SECBIP_JAR} -I${BIP_DIR} -p ${BIPP} -d "${BIPD}()" \
            || error "During Secure BIP conversion"
        mv secureBip.bip ${OUTPUT}/
    fi
}

compileBip() {
    local NAME=$1
    local GENDIR=$2
    local OUTPUT=$3

    if pushd ${OUTPUT}; then
        if ! test -d build; then
            mkdir build || error
            cd build  || error
            cmake ../  || error
        else
            cd build  || error
            cmake ../  || error
        fi
        make VERBOSE=1 -j 4  || error
        popd
    fi

    test -e ${OUTPUT}/build/system || error
    cp -pu ${OUTPUT}/build/system ${GENDIR}/${NAME}
}

genBip() {
    local BIP_DIR=$1
    local BIPP=$2
    local BIPD=$3
    local GENDIR=${4}
    local OUTPUT=${GENDIR}/bipc

    test x"${BIP2_ENGINE_LIB_DIR}" != x"" || error "BIP environment not loaded"

    echo test ${BIP_DIR}/${BIPP}.bip -nt ${OUTPUT}/CMakeLists.txt -o ! -e ${OUTPUT}/${BIPP}.bip
    if test ${BIP_DIR}/${BIPP}.bip -nt ${OUTPUT}/CMakeLists.txt -o ! -e ${OUTPUT}/${BIPP}.bip ; then
        rm -rf ${OUTPUT}
        mkdir -p ${OUTPUT} || error  "Could not generate output directory"
        cp -p ${BIP_DIR}/${BIPP}.bip ${OUTPUT}/${BIPP}.bip

        bipc.sh -I ${BIP_DIR} -p ${BIPP} -d "${BIPD}()" \
                --gencpp-cc-I ${SECBIP_DIR}/ \
                --gencpp-output ${OUTPUT}  || error

        if pushd ${OUTPUT}; then
            local EXTRA="-D_GLIBCXX_USE_CXX11_ABI=0"
            if ! ( c++ -no-pie  2>&1 | grep -q no-pie ) ; then
                EXTRA="${EXTRA} -no-pie"
            fi

            mkdir -p instrumentation
            cp -p ${SECBIP_DIR}/instrumentation/*.{hpp,cpp} instrumentation/
            patch CMakeLists.txt  <<EOF
    7,10c7,10
    < SET(CMAKE_CXX_FLAGS "-Wall -std=c++0x")
    < SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O3 -g -std=c++0x")
    < SET(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG -std=c++0x")
    < SET(CMAKE_CXX_FLAGS_DEBUG  "-O0 -g -std=c++0x")
    ---
    > SET(CMAKE_CXX_FLAGS "-Wall -std=c++0x -g ${EXTRA}")
    > SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O3 -g -std=c++0x ${EXTRA}")
    > SET(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG -std=c++0x ${EXTRA}")
    > SET(CMAKE_CXX_FLAGS_DEBUG  "-O0 -g -std=c++0x ${EXTRA}")
EOF
            sed -i -e s~'/${SECBIP_DIR}/'~"${SECBIP_DIR}"~g secureBip/CMakeLists.txt
            for file in secureBip/include/secureBip/*.hpp; do
                sed -i -e s~'/${SECBIP_DIR}/'~"${SECBIP_DIR}"~g "$file"
            done
            popd
        fi
    fi

    compileBip ${BIPP} ${GENDIR} ${OUTPUT}
}

checkResults() {
    local log="$1"
    local db="$2"
    test -r "$log" || error "No results"
    test -r ${db} && cat ${db}

    python <<EOF
import json
try:
  filed = open("${log}", "r")
  stats = json.load(filed)
  print(stats)
except:
  print("no stats")
EOF
}

stopAll() {
    local PID="$1"
    local log="$2"
    local db="$3"
    echo "Interruption, wait a few seconds..."
    sleep 0.5
    checkResults ${log} ${db}
    sleep 2
    kill -9 $PID 2>/dev/null
    exit 0
}

monitorBip() {
    local ttime="$1"
    local stime="$2"
    local PID="$3"

    local json=bip_lub_stats.json
    local statusLog=bip_lub.log

    test "$ttime" -gt "0" || error "Total time not provided"
    test "$stime" -gt "0" || error "Status time not provided"
    test "$ttime" -gt "$stime" || error "Status time ($stime) too high (>$ttime)"

    trap "stopAll $PID $json $statusLog"  SIGTERM SIGQUIT SIGINT

    elapsed=0
    while test "$elapsed" -le "$ttime"; do
        sleep $stime
        elapsed=$(( $elapsed + $stime ))
        kill -USR2 $PID > /dev/null
        echo "Snapshot!" > /dev/stderr
        test -r "${LOGF}" && tail -5 "${LOGF}"   > /dev/stderr
        test -r ${json}      && cat ${json}      > /dev/stderr
        test -r ${statusLog} && cat ${statusLog} > /dev/stderr
    done

    stopAll $PID $json $statusLog
}

runBip() {
    local execf="$1"
    local ttime="$2"
    local stime="$3"
    test -x $execf || error "Not compiled"

    LOGF=./system.log
    $execf <&1 & PID=$!

    monitorBip $ttime $stime $PID &

    fg $(jobs '$execf' | sed s/'\[\([0-9]*\)\].*$'/'%\1'/g)
    wait $PID
}

if test $# -le 2; then
    error "Usage: $0 <bip directory> <bip package> <bip name> [<timeout>] [<snapshot time>]"
else
    BIP_DIR=$1
    BIPP=$2
    BIPD=$3
    if test $# -gt 3; then
        TIMEOUT=$4
        if test $# -gt 4; then
            SNAPSHOT=$5
        fi
    fi
fi

loadEnv
runSecureBip ${BIP_DIR} ${BIPP} ${BIPD} ${GEN_DIR}
genBip ${GEN_DIR} secureBip secureBip ${GEN_DIR}

runBip ${GEN_DIR}/secureBip ${TIMEOUT} ${SNAPSHOT}
