# RV-Secure BIP

The SecureBIP tool is a prototype tool based on the RV-secureBIP framework that automatically verifies and enforces the information flow security in a component based model by monitoring the behaviour of the system. 

The model is partially labelled with security annotations and we run a static analysis of system dependencies.
It also introduces a monitor component that stores and propagates security levels of the model to maintain the information flow security.

## Getting Started

The SecureBIP prototype purpose is to demonstrate the secure instrumentation and runtime control over a real example. It takes an BIP model in input, compile it and run the application with the security monitor. A basic example (simpleTransition.bip) is provided.

### Prerequisites

The SecureBIP prototype has been tested on Linux systems (Ubuntu 16.04, Debian 8 and 9) and requires the same execution context as the [BIP Compiler (Download)](http://www-verimag.imag.fr/New-BIP-tools.html):

 - a fresh and operational installation of the BIP tools,
 - a recent JVM (5.0 or upper) mandatory,
 - a C++ compiler (tested with gcc 4.8 and 6.3),
 - the standard build tools: cmake, make.

A more detailed description of the BIP compiler is provided in the [documentation](http://www-verimag.imag.fr/TOOLS/DCS/bip/doc/latest/html/installing-using-compiler.html). The SecureBIP driver takes in charge a part of the setup process.

A Bash script automate the code generation, the compilation, and a controlled execution.

### Installing

The SecureBIP package is composed of the following file tree:

    ├── SecureBIP
    │   └── SecureBIPSynthesis.jar
    │   └── bip-full
    │       └── <Link or installation location of standard BIP tools>
    ├── secureBip.sh
    ├── README.md
    ├── acts.txt
    └── simpleTransition.bip
    

The first installation step is to extract these files in a fresh directory. Then, the tool requires a fully operational installation of the BIP tools (see [BIP Tools](http://www-verimag.imag.fr/BIP-Tools-93.html?lang=en)) in the directory *SecureBIP/bip-full*. The *bip-full* directory can also be a symbolic link to an pre-existing installation.

### Standard Usage

The SecureBIP tool is operated by the driver script *secureBip.sh*. Its execution gives a summary of its usage:

```
    Usage: ./secureBip.sh <bip directory> <bip package> <bip name> [<timeout>] [<snapshot time>]
```

The minimum set of options taken are the following:
    <bip directory>: the location of the input bip file
    <bip package>:  a package name to compile (see BIP documentation)
    <bip name>: a declaration name (see BIP documentation)

Optionally, two extra parameters can be provided to control the system execution:
    [<timeout>]: the execution time-out (in case of infinite execution)
    [<snapshot time>]: the time between two security snapshots.

## Internal description of the System

The SecureBIP tool is composed of tree modules:
- a bip translator,
- a runtime library for monitoring security labels,
- a driver script.

### The BIP translator

The BIP Translator is implemented in JAVA and includes the Java libraries necessary for the instrumentation of BIP models. The system generate the instrumented code using the ANTLR StringTemplate system.

### The Runtime Library

The runtime library is a C++ system implementing the verification of security label interferences using the Least-Upper-Bound method. A simple API initialize and operate the computation at Runtime. A mechanism can be activated for dynamically updating the dependencies by the user (a message is prompted).

### The Driver Script

The driver script operates the basic initialization, compilation, and execution steps:
- it checks and loads the bip environment,
- it launches the instrumentation of the original BIP model,
- it generates and compiles the secure BIP model with the standard BIP tools,
- it runs the generated application in a controlled mode able to operate the user interactions.

## Running the tests

A Basic test is provided with the *simpleTransition.bip* file. To run the test, simply run the following command:

```
    $ ./secureBip.sh ./ simpleTransition simpleTransition
```

## License

This project is licensed and copyrighted by INRIA Rennes and provided for testing purpose, all other right reserved - contact the authors for more information.
